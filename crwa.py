#!/usr/bin/env python
#
# crwa.py - collectd-rrdtool-WSGI adapter
#
# A trivial thing that can serve rrdtool-drawn images of
# collectd-supplied data over HTTP.


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


import collections
import io
import json
import os
import re
import socket
import subprocess


try:
    import urllib.parse as url_parse
except ImportError:
    import urlparse as url_parse


BASEDIR = "/var/lib/collectd"
COLOR_SEQUENCE = [
    "aa0000",
    "999900",
    "00aa00",
    "0000aa",
    "880088",
]
CONTENT_TYPE_PNG = "image/png"
RE_DF_DIR = "df-(.+)"
RE_HDDTEMP_FILE = "temperature-(.+).rrd"


PlotOpts = collections.namedtuple("PlotOpts", ("hostname",
                                               "attribute_title",
                                               "duration",
                                               "width",
                                               "height"))


class Request:
    def __init__(self, method, path, query):
        self.method = method
        self.path = path
        self.query = query

    def description(self):
        with io.StringIO() as buf:
            print("{} {}".format(self.method, "/".join(self.path)),
                  file=buf)
            for query_key in sorted(self.query.keys()):
                values = self.query[query_key]

                print("  {}: {}".format(query_key, values),
                      file=buf)
            return buf.getvalue()

    @staticmethod
    def from_environ(environ):
        method = environ["REQUEST_METHOD"]
        path = path_from_environ(environ)
        query = query_from_environ(environ)

        return Request(method, path, query)

    def path_ends_with(self, *seq):
        seq_len = len(seq)
        path_len = len(self.path)

        skip_cnt = path_len - seq_len
        if skip_cnt < 0:
            return False

        for n in range(seq_len):
            if self.path[n + skip_cnt] != seq[n]:
                return False

        return True

    def query_get_one(self, key, fallback=None):
        items = self.query.get(key, [])
        if len(items) == 0:
            if fallback is not None:
                return fallback

            msg = "{} was not in query params".format(key)
            raise KeyError(msg)

        return items[0]


class Response:
    def __init__(self, status, content_type, body_bytes):
        self.status = str(status)
        self.content_type = str(content_type)
        self.body = body_bytes

    @staticmethod
    def not_found_text(text):
        return Response("404 Not Found", "text/plain", text.encode("utf-8"))

    @staticmethod
    def ok_bin(content_type, body):
        return Response("200 OK", content_type, body)

    @staticmethod
    def ok_json(o):
        body = json.dumps(o).encode("utf-8")
        return Response("200 OK", "application/json", body)

    @staticmethod
    def ok_text(text):
        return Response("200 OK", "text/plain", text.encode("utf-8"))


def color_sequence():
    n = 0
    seq_len = len(COLOR_SEQUENCE)

    while True:
        yield COLOR_SEQUENCE[n]
        n = (n + 1) % seq_len


def df_dirs_of_host(hostname):
    rrd_dir = rrddir_of_host(hostname)
    if not os.path.isdir(rrd_dir):
        return {}

    result = {}

    for entry in os.listdir(rrd_dir):
        mountname_match = re.search(RE_DF_DIR, entry)
        if mountname_match is None:
            continue

        mountname = mountname_match.group(1)

        full_path = "{}/{}".format(rrd_dir, entry)
        if not os.path.isdir(full_path):
            continue

        result[mountname] = full_path

    return result


def handle_request(request):
    """Handle a single HTTP request.

    :param request: HTTP request from client
    :type request: Request
    :return: HTTP response to be sent to client
    :rtype: Response
    """
    if request.path == ["api", "png"]:
        hostname = request.query_get_one("hostname", socket.getfqdn())
        duration = int(request.query_get_one("duration", "86400"))
        attr = request.query_get_one("attr", socket.getfqdn())
        width = int(request.query_get_one("width", "800"))
        height = int(request.query_get_one("height", "150"))

        plot_opts = PlotOpts(hostname, attr, duration, width, height)


        if attr == "load":
            return Response.ok_bin(CONTENT_TYPE_PNG,
                                   png_of_load(hostname, plot_opts))
        elif attr == "memory":
            return Response.ok_bin(CONTENT_TYPE_PNG,
                                   png_of_memory(hostname, plot_opts))
        elif attr == "paging":
            return Response.ok_bin(CONTENT_TYPE_PNG,
                                   png_of_paging(hostname, plot_opts))
        elif attr == "paging_avg":
            return Response.ok_bin(CONTENT_TYPE_PNG,
                                   png_of_paging_avg(hostname, plot_opts))
        elif attr == "swap":
            return Response.ok_bin(CONTENT_TYPE_PNG,
                                   png_of_swap(hostname, plot_opts))
        elif attr == "hddtemp":
            return Response.ok_bin(CONTENT_TYPE_PNG,
                                   png_of_hddtemp(hostname, plot_opts))
        elif attr.startswith("df-"):
            mountname = attr[3:]
            return Response.ok_bin(CONTENT_TYPE_PNG,
                                   png_of_df(hostname, mountname, plot_opts))
        else:
            return Response.not_found_text("{} is not supported".format(attr))
    elif request.path == ["web", "index.html"]:
        with open("web/index.html", "rb") as f:
            body = f.read()
        return Response.ok_bin("text/html", body)
    elif request.path == ["web", "index.js"]:
        with open("web/index.js", "rb") as f:
            body = f.read()
        return Response.ok_bin("text/javascript", body)
    elif request.path == ["api", "hosts"]:
        return Response.ok_json(hosts())
    elif request.path_ends_with("echo"):
        return Response.ok_text(request.description())
    else:
        return Response.not_found_text("This resource is not available\n")


def path_from_environ(environ):
    def nonempty(x):
        return x is not None

    def sanitized_item(item):
        stripped = item.strip()
        if 0 == len(stripped):
            return None

        return stripped

    raws = environ["PATH_INFO"].split("/")

    return list(filter(nonempty, map(sanitized_item, raws)))


def png_of_df(hostname, mount, plot_opts):
    fnames = rrdfiles_of_df(hostname)[mount]

    rrdgraph_cmd = rrdgraph_common(plot_opts) + [
        "-l", "0",
        "DEF:free={}:value:MIN".format(fnames["free"]),
        "DEF:used={}:value:MAX".format(fnames["used"]),
        "CDEF:sum=free,used,+",
        "AREA:sum#0022aa55:free\l",
        "AREA:used#882222:used\l",
    ]

    return subprocess.check_output(rrdgraph_cmd)


def png_of_hddtemp(hostname, plot_opts):
    fnames = rrdfiles_of_hddtemp(hostname)

    disk_names = sorted(fnames.keys())

    def def_for_disk(disk_name):
        file_name = fnames[disk_name]
        return "DEF:{}={}:value:AVERAGE".format(disk_name, file_name)

    data_defs = [def_for_disk(disk) for disk in disk_names]

    color = color_sequence()

    def line_for_disk(disk_name):
        return "LINE2:{}#{}:{}".format(disk_name, next(color), disk_name)

    line_defs = [line_for_disk(disk) for disk in disk_names]

    rrdgraph_cmd = rrdgraph_common(plot_opts) + [
        "-l", "0",
    ]

    return subprocess.check_output(rrdgraph_cmd + data_defs + line_defs)


def png_of_load(hostname, plot_opts):
    fname = rrdfile_of_load(hostname)

    rrdgraph_cmd = rrdgraph_common(plot_opts) + [
        "-l", "0",
        "DEF:load={}:shortterm:AVERAGE".format(fname),
        "DEF:loadmax={}:shortterm:MAX".format(fname),
        "VDEF:loadlast=load,LAST",
        "VDEF:loadavg=load,AVERAGE",
        "VDEF:loadmaxmax=loadmax,MAXIMUM",
        "AREA:load#FF000080:1 minute load average",
        "GPRINT:loadlast:now\\: %3.2lf",
        "GPRINT:loadavg:average\\: %3.2lf\\l",
        "LINE:loadmax#FF0000FF:1 minute load average",
        "GPRINT:loadmaxmax:max\\: %3.2lf\\l",
    ]

    return subprocess.check_output(rrdgraph_cmd)


def png_of_memory(hostname, plot_opts):
    fnames = rrdfiles_of_memory(hostname)
    free = fnames["free"]
    used = fnames["used"]
    bufd = fnames["buffered"]
    cahd = fnames["cached"]

    rrdgraph_cmd = rrdgraph_common(plot_opts) + [
        "-l", "0",
        "DEF:free={}:value:AVERAGE".format(free),
        "VDEF:free_last=free,LAST",
        "VDEF:free_avg=free,AVERAGE",
        "DEF:used={}:value:AVERAGE".format(used),
        "DEF:used_max={}:value:MAX".format(used),
        "VDEF:usedlast=used_max,LAST",
        "VDEF:usedavg=used,AVERAGE",
        "VDEF:used_maxmax=used_max,MAXIMUM",
        "DEF:bufd={}:value:AVERAGE".format(bufd),
        "VDEF:bufd_last=bufd,LAST",
        "VDEF:bufd_avg=bufd,AVERAGE",
        "DEF:cahd={}:value:AVERAGE".format(cahd),
        "VDEF:cahd_last=cahd,LAST",
        "VDEF:cahd_avg=cahd,AVERAGE",
        "AREA:used#882222:used    ",
        "GPRINT:usedlast:%4.2lf%S",
        "GPRINT:usedavg:average\\: %4.2lf%S",
        "GPRINT:used_maxmax:max\\: %4.2lf%S\\l",
        "STACK:free#0022aa55:unused  ",
        "GPRINT:free_last:%4.2lf%S",
        "GPRINT:free_avg:average\\: %4.2lf%S\\l",
        "STACK:cahd#009900:cached  ",
        "GPRINT:cahd_last:%4.2lf%S",
        "GPRINT:cahd_avg:average\\: %4.2lf%S\\l",
        "STACK:bufd#006600:buffers ",
        "GPRINT:bufd_last:%4.2lf%S",
        "GPRINT:bufd_avg:average\\: %4.2lf%S\\l",
        "LINE1:used_max#ff0000",
    ]

    return subprocess.check_output(rrdgraph_cmd)


def png_of_paging(hostname, plot_opts):
    file_names = rrdfiles_of_swap(hostname)

    io_in = file_names["in"]
    io_out = file_names["out"]

    rrdgraph_cmd = rrdgraph_common(plot_opts) + [
        "DEF:in_avg={}:value:AVERAGE".format(io_in),
        "VDEF:in_avg_last=in_avg,LAST",
        "VDEF:in_avg_avg=in_avg,AVERAGE",
        "VDEF:in_avg_max=in_avg,MAXIMUM",
        "DEF:in_max={}:value:MAX".format(io_in),
        "VDEF:in_max_max=in_max,MAXIMUM",
        "DEF:out_avg={}:value:AVERAGE".format(io_out),
        "VDEF:out_avg_last=out_avg,LAST",
        "VDEF:out_avg_avg=out_avg,AVERAGE",
        "VDEF:out_avg_max=out_avg,MAXIMUM",
        "DEF:out_max={}:value:MAX".format(io_out),
        "VDEF:out_max_max=out_max,MAXIMUM",
        "CDEF:nin_avg=in_avg,-1,*",
        "CDEF:nin_max=in_max,-1,*",
        "AREA:out_avg#88000088:swap out       ",
        "GPRINT:out_avg_last:now\\: %4.1lf%spages/s",
        "GPRINT:out_avg_avg:avg\\: %4.1lf%spages/s",
        "GPRINT:out_avg_max:max\\: %4.1lf%spages/s\\l",
        "AREA:nin_avg#00008888:swap in        ",
        "GPRINT:in_avg_last:now\\: %4.1lf%spages/s",
        "GPRINT:in_avg_avg:avg\\: %4.1lf%spages/s",
        "GPRINT:in_avg_max:max\\: %4.1lf%spages/s\\l",
        "LINE1:out_max#880000ff:swap out max   ",
        "GPRINT:out_max_max:max\\: %4.1lf%spages/s\\l",
        "LINE1:nin_max#000088ff:swap in max    ",
        "GPRINT:in_max_max:max\\: %4.1lf%spages/s\\l",
    ]

    return subprocess.check_output(rrdgraph_cmd)


def png_of_paging_avg(hostname, plot_opts):
    file_names = rrdfiles_of_swap(hostname)

    io_in = file_names["in"]
    io_out = file_names["out"]

    rrdgraph_cmd = rrdgraph_common(plot_opts) + [
        "DEF:in_avg={}:value:AVERAGE".format(io_in),
        "VDEF:in_avg_last=in_avg,LAST",
        "VDEF:in_avg_avg=in_avg,AVERAGE",
        "VDEF:in_avg_max=in_avg,MAXIMUM",
        "DEF:out_avg={}:value:AVERAGE".format(io_out),
        "VDEF:out_avg_last=out_avg,LAST",
        "VDEF:out_avg_avg=out_avg,AVERAGE",
        "VDEF:out_avg_max=out_avg,MAXIMUM",
        "CDEF:nin_avg=in_avg,-1,*",
        "AREA:out_avg#88000088:swap out       ",
        "GPRINT:out_avg_last:now\\: %4.1lf%spages/s",
        "GPRINT:out_avg_avg:avg\\: %4.1lf%spages/s",
        "GPRINT:out_avg_max:max\\: %4.1lf%spages/s\\l",
        "AREA:nin_avg#00008888:swap in        ",
        "GPRINT:in_avg_last:now\\: %4.1lf%spages/s",
        "GPRINT:in_avg_avg:avg\\: %4.1lf%spages/s",
        "GPRINT:in_avg_max:max\\: %4.1lf%spages/s\\l",
    ]

    return subprocess.check_output(rrdgraph_cmd)


def png_of_swap(hostname, plot_opts):
    file_names = rrdfiles_of_swap(hostname)

    used = file_names["used"]
    free = file_names["free"]

    rrdgraph_cmd = rrdgraph_common(plot_opts) + [
        "-l", "0",
        "DEF:used={}:value:AVERAGE".format(used),
        "VDEF:used_last=used,LAST",
        "DEF:free={}:value:AVERAGE".format(free),
        "VDEF:free_last=free,LAST",
        "AREA:used#ff000088:used",
        "GPRINT:used_last:%4.2lf%s\\l",
        "STACK:free#0022aa55:free",
        "GPRINT:free_last:%4.2lf%s\\l",
    ]

    return subprocess.check_output(rrdgraph_cmd)


def query_from_environ(environ):
    query_string = environ.get("QUERY_STRING", "")
    return url_parse.parse_qs(query_string)


def rrddir_of_host(hostname):
    return "{}/rrd/{}".format(BASEDIR, hostname)


def rrdfile_of_load(hostname):
    rrddir = rrddir_of_host(hostname)

    return "{}/load/load.rrd".format(rrddir)


def rrdfiles_of_df(hostname):

    df_dirs = df_dirs_of_host(hostname)

    result = {}

    for mountname in df_dirs.keys():
        mount_dfdir = df_dirs[mountname]
        free_rrd = "{}/df_complex-free.rrd".format(mount_dfdir)
        used_rrd = "{}/df_complex-used.rrd".format(mount_dfdir)

        if not os.path.isfile(free_rrd):
            continue

        if not os.path.isfile(used_rrd):
            continue

        rrds = {
            "free": free_rrd,
            "used": used_rrd,
        }

        result[mountname] = rrds

    return result


def rrdfiles_of_hddtemp(hostname):
    rrddir = rrddir_of_host(hostname)

    htdir = "{}/hddtemp".format(rrddir)
    if not os.path.isdir(htdir):
        return []

    result = {}

    for entry in os.listdir(htdir):
        diskname_match = re.search(RE_HDDTEMP_FILE, entry)
        if diskname_match is None:
            continue

        diskname = diskname_match.group(1)

        fullpath = "{}/{}".format(htdir, entry)

        if not os.path.isfile(fullpath):
            continue

        result[diskname] = fullpath

    return result


def rrdfiles_of_memory(hostname):
    rrddir = rrddir_of_host(hostname)

    mem = "{}/memory".format(rrddir)
    return {
        "used": "{}/memory-used.rrd".format(mem),
        "free": "{}/memory-free.rrd".format(mem),
        "buffered": "{}/memory-buffered.rrd".format(mem),
        "cached": "{}/memory-cached.rrd".format(mem),
    }


def rrdfiles_of_swap(hostname):
    swap_dir = "{}/swap/".format(rrddir_of_host(hostname))

    return {
        "in": "{}/swap_io-in.rrd".format(swap_dir),
        "out": "{}/swap_io-out.rrd".format(swap_dir),
        "used": "{}/swap-used.rrd".format(swap_dir),
        "free": "{}swap-free.rrd".format(swap_dir),
    }


def rrdgraph_common(plot_opts):
    title = "{} at {}".format(plot_opts.attribute_title, plot_opts.hostname)
    return [
        "rrdtool", "graph", "-",
        "-w", "{}".format(plot_opts.width),
        "-h", "{}".format(plot_opts.height),
        "--end", "now",
        "--start", "end-{}s".format(plot_opts.duration),
        "--title", title,
    ]


def hosts():
    return os.listdir("/var/lib/collectd/rrd")


def wsgi_application():
    def app(environ, start_response):
        request = Request.from_environ(environ)
        response = handle_request(request)

        body_len = len(response.body)

        headers = [
            (str("Content-Length"), str(body_len)),
            (str("Content-Type"), response.content_type),
            ]

        start_response(response.status, headers)
        return [response.body]

    return app

if __name__.startswith("_mod_wsgi_"):
    # For mod_wsgi
    application = wsgi_application()
elif __name__ == "__main__":
    # Try to run a standalone instance.
    from wsgiref.simple_server import make_server

    httpd = make_server("", 8080, wsgi_application())
    httpd.serve_forever()
