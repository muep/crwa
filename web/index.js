DURATIONS = {
    "hour": 3600,
    "hours": 28800,
    "day": 100000,
    "week": 1000000,
    "month": 3000000,
}

function clear_element(element) {
    if (element === null) {
        return;
    }

    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

function data_area_init() {
    var data_area = document.getElementById("data_area");
    var dur_sel = document.getElementById("duration_selector");

    var hash = window.location.hash;

    clear_element(data_area);
    if (hash.length == 0) {
        /* No host selected. No data then either. */
        return;
    }

    var hostname = hash.substring(1);
    var duration = DURATIONS[dur_sel.value];

    data_area.appendChild(h1_with_text(hostname));
    data_area.appendChild(img_element(hostname, "load", duration));
    data_area.appendChild(img_element(hostname, "memory", duration));
    data_area.appendChild(img_element(hostname, "paging", duration));
    data_area.appendChild(img_element(hostname, "swap", duration));
}

function duration_select_init() {
    var dur_sel = document.getElementById("duration_selector");
    dur_sel.onchange = data_area_init;
}

function encoded_url(url, params) {
    var query_string = "";

    for (var k in params) {
        var v = params[k];

        var pair = encodeURIComponent(k) + "=" + encodeURIComponent(v);
        query_string += "&" + pair;
    }

    if (query_string.length > 0) {
        return url + "?" + query_string.substring(1);
    }

    return url;
}

function img_element(hostname, attr, duration) {
    var url = encoded_url("../api/png", {
        hostname: hostname,
        attr: attr,
        duration: "" + duration,
        width: "" + window.innerWidth - 200,
    });

    var img = document.createElement("img");
    img.src = url;
    img.alt = attr + " at " + hostname;
    return img;
}

function h1_with_text(txt) {
    var h1 = document.createElement("h1");
    var text = document.createTextNode(txt);
    h1.appendChild(text);
    return h1;
}

function h2_with_text(txt) {
    var h2 = document.createElement("h2");
    var text = document.createTextNode(txt);
    h2.appendChild(text);
    return h2;
}

function hash_changed() {
    data_area_init();
}

function host_list_fetch() {
    var async = true;
    var data = null;

    var req = new XMLHttpRequest();

    req.onreadystatechange = function() {
        if (req_ok(req)) {
            var hosts = JSON.parse(req.responseText);
            hosts.sort();
            host_list_update(hosts);
        }
    };

    req.open('GET', '../api/hosts', async);
    req.send(data);
}

function host_list_update(hosts) {
    var host_count = hosts.length;

    var hostlist = document.getElementById("hostlist");
    clear_element(hostlist)

    var ul = document.createElement("ul");

    for (var i = 0; i < host_count; ++i) {
        var name = hosts[i];

        var textnode = document.createTextNode(name);

        var a = document.createElement("a");
        a.appendChild(textnode);
        a.href = "#" + name;

        var li = document.createElement("li");
        li.appendChild(a);

        ul.appendChild(li);
    }

    hostlist.appendChild(ul);
}

function req_ok(req) {
    if (req.readyState != XMLHttpRequest.DONE) {
        return false;
    }
    if (req.status != 200) {
        return false;
    }

    return true;
}

/* This is expected to get called once, right after the initial
 * document from index.html is loaded. */
function main() {
    window.onhashchange = hash_changed;

    duration_select_init();

    /* Immediately initiate refresh of host list */
    host_list_fetch();

    /* Also initialize the data area */
    data_area_init();
}
